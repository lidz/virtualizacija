<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp');

/** MySQL database username */
define('DB_USER', 'wp');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'JO/1H%vD~pc7nsw^:_G``%H*?:9$%npR=H^X;5#p*4^!~/PQQP7g(WD]] P&X0{N');
define('SECURE_AUTH_KEY',  '3<$TZ?9ws=au>VJ6Wul)]E!V3R(-`o9DF71Ko3_t<xxW7Kvi~nlGY;!3-,g0I;LC');
define('LOGGED_IN_KEY',    'sO02[CD.b7b7-AV>%MaVP*>4~ *QMIQLD6EmP0~2PY9JqAEeV?Bg^gepokhD0ZM.');
define('NONCE_KEY',        '8s}ym~]$#:kmAZ]=_$vK2sb5etm&H bW=}$&yI0;/*8P6p~jgW(hRQ)|L*(;P6}{');
define('AUTH_SALT',        'Tur13e2R~i/[V8,m,=dWnU,g,|$Vh%oBBj{R`x}1#9~tt{s5{-O)yZZ4`I3;*8*z');
define('SECURE_AUTH_SALT', '!kZZU}krDHYa5P+lV7E~{I k/uX@L.=k}&#B^w;/?tg/;F[;m]1fy;rR>%t1drEi');
define('LOGGED_IN_SALT',   ' zeA:9 {cIUULAr_?dw.uTm}3IUl1b$SGF..Mni>99kzilo7;g?+V!<bPVPeY2g#');
define('NONCE_SALT',       'ha$SWSzrILl~QyM}(2%i9AKU:-5_f+ww-xD}$wKKV)kW9loFn]~vvOd%^o1z31K?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
