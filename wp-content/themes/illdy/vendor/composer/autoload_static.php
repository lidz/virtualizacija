<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit3a12cac3d0cfb312c2b86d07f5d76323
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'PhpXmlRpc\\' => 10,
            'PHPOneAPI\\' => 10,
        ),
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'PhpXmlRpc\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpxmlrpc/phpxmlrpc/src',
        ),
        'PHPOneAPI\\' => 
        array (
            0 => __DIR__ . '/..' . '/iddqdby/phponeapi/src',
        ),
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit3a12cac3d0cfb312c2b86d07f5d76323::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit3a12cac3d0cfb312c2b86d07f5d76323::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
