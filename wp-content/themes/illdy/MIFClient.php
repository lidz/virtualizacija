<?php

namespace App;

use PHPOneAPI\Client;

class MIFClient {
    private static $client; 

    private static function setupClient() {
        MIFClient::$client = new Client('lidz2239', base64_decode('QW1uMzUxNDA='), 'grid5.mif.vu.lt', true, '', 'cloud3/RPC2');
    }

    private static function generatePassword() {
        return substr(str_shuffle(strtolower(sha1(rand() . time() . "9454d3r9"))),0, 8);
    }

    public static function getVMNames() {
        MIFClient::setupClient();
        $xml = MIFClient::$client->call('one.vmpool.info', [-3, -1, -1, -1]);
        
        $vmpool = simplexml_load_string($xml)->VM;
        $vms = array();
        
        foreach ($vmpool as $vm) {
            array_push($vms, $vm->NAME);
        }

        return $vms;
    }

    public static function getVMInfo($id) {
        MIFClient::setupClient();
        $xml = MIFClient::$client->call('one.vm.info', [$id]);

        return (string) simplexml_load_string($xml)->USER_TEMPLATE->TCP_PORT_FORWARDING;
    }

    public static function createVM($username) {
        MIFClient::setupClient();
        $password = MIFClient::generatePassword();
        $id = MIFClient::$client->call('one.template.instantiate', [138, '', false, 'CONTEXT=[NETWORK=YES,SWAP_SIZE=512,USER_NAME='.$username.',USER_PASSWORD='.$password.']', false]);
        sleep(15);
        $info = MIFClient::getVMInfo($id);
        return [$id, $info, $password];
    }
}
